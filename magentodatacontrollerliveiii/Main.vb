﻿Imports System
Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Security.Permissions
Imports MySql.Data.MySqlClient
Imports System.Xml
Imports System.Threading

Public Class Main
    Public Sub Main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "Magento Comparitor Controller III - Live " & String.Format("Version {0}", My.Application.Info.Version.ToString)
        If File.Exists("C:\DBControllerLiveArchive\Restarted.log") Then
            flgRestarted = True
            Kill("C:\DBControllerLiveArchive\Restarted.log")
        End If

        If flgRestarted = False Then
            'Tracing("Program Started")
        Else
            'Tracing("Program Restarted")
        End If
        'Load DataBase Connections
        Load_Database_Connections()
        If flgFailed = True Then
            lblMessage.Text = "Controller Failed to start"
            lblMessage.Refresh()
            'Tracing("Service Failed to Start")
            Exit Sub
        End If

        'Set the properties on the monitor.
        FileMonitor.Path = "C:\DBControllerLive"
        FileMonitor.Filter = "*.*"
        FileMonitor.IncludeSubdirectories = False
        FileMonitor.NotifyFilter = NotifyFilters.LastAccess Or NotifyFilters.LastWrite Or NotifyFilters.FileName Or NotifyFilters.DirectoryName


        If flgRestarted = True Then
            'if restarted, check for remaining files
            btnStart_Click(Me, e)
            'If restarted, check for orphaned skus
            Orphans()
        Else
            'Begin monitoring.
            'Tracing("Service Started")
            FileMonitor.EnableRaisingEvents = True
            Timer1.Start()
            SyncTimer.Start()
        End If
    End Sub
    Private Sub FileMonitor_Created(sender As Object, e As FileSystemEventArgs) Handles FileMonitor.Created
        Timer1.Stop()
        SyncTimer.Stop()
        TextFileHandler(e.FullPath)
        lblMessage.Text = "Finished"
        lblMessage.Refresh()
        Timer1.Start()
        SyncTimer.Start()
    End Sub
    Sub Load_Database_Connections()
        Dim lngkey As Long = 62382737 ' Used for encryption
        Dim connMaster As New MySqlConnection(connstr)
        Try
            If connMaster.State = ConnectionState.Closed Then connMaster.Open()
        Catch ex As Exception
            'Tracing("error with connection, tring to re-open")
            connMaster.Close()
            'second attempt
            Try
                connMaster.Open()
            Catch
                SendEmail("Connection failed when tring to get Database List Information")
                'Tracing("connection failed to open when getting database connection information")
                flgFailed = True
                Exit Sub
            End Try
        End Try
        Dim query As String = "SELECT * FROM mverp_db_adjustments_stores"
        Dim cmdStoresAddEdit As New MySqlCommand(query, connMaster)
        Dim reader As MySqlDataReader
        Try
            reader = cmdStoresAddEdit.ExecuteReader
            DatabaseList.Clear()
            If reader.HasRows = True Then
                While reader.Read
                    Dim tmpDatabase As New Database
                    tmpDatabase.StoreID = reader("id")
                    tmpDatabase.StoreName = reader("store_name")
                    tmpDatabase.DatabaseConnection = "Database=" & reader("db_connection_dbname") & ";Data Source=" & reader("db_connection_source") & ";User Id=" & reader("db_connection_user") & ";Password=" & StrDecode(reader("db_connection_pwd"), lngkey, False)
                    tmpDatabase.PHPWebsite = reader("php_webpage_path")
                    DBConnections.Add(tmpDatabase.StoreName, tmpDatabase.DatabaseConnection)
                    PHPStrings.Add(tmpDatabase.StoreID, tmpDatabase.PHPWebsite)
                    StoreIDs.Add(tmpDatabase.StoreID, tmpDatabase.StoreName)
                    StoreNames.Add(tmpDatabase.StoreName, tmpDatabase.StoreID)
                    DatabaseList.Add(tmpDatabase)
                End While
            End If
        Catch ex As Exception
            SendEmail("Connection failed when tring to get Database List Information")
            'Tracing("Connection failed when tring to get Database List Information")
        Finally
            reader.Close()
            cmdStoresAddEdit.Dispose()
            connMaster.Close()
        End Try
    End Sub
    Sub TextFileHandler(ByVal TextFile As String)
        'Dim AtSymbol As Integer ' = InStr(e.FullPath, "@") + 1
        'Dim TythSymbol As Integer '' = InStr(e.FullPath, "-") + 1
        'Dim PeriodSymbol As Integer ' = InStr(e.FullPath, ".")
        'Get Source
        'Dim DBSource As String ' =  Mid(e.FullPath, AtSymbol, PeriodSymbol - AtSymbol)
        Dim TargetFilename As String
        'Dim OrderID As String
        'Tracing("Entered TextFileHandler")
        flgFailed = False

        'Get File name
        'AtSymbol = InStr(TextFile, "@") + 1
        'TythSymbol = InStr(TextFile, "~") + 1
        'PeriodSymbol = InStr(TextFile, ".")
        TargetFilename = Mid(TextFile, InStrRev(TextFile, "\") + 1)
        'OrderID = Mid(TextFile, TythSymbol, (AtSymbol - 1) - TythSymbol)
        'If Trim(OrderID) = "" Then
        '    'no ordernumber or string found
        '    SendEmail("Bad txt file found. - " & TextFile & " - Moved to archive.")
        '    'Tracing("Bad txt file found. - " & TextFile & " - Moved to archive.")
        '    MoveFile(TargetFilename)
        '    Exit Sub
        'End If

        'Get call source
        'DBSource = Mid(TextFile, AtSymbol, PeriodSymbol - AtSymbol)
        'Process inital call
        'Tracing("First call to processfiles")
        lblMessage.Text = "Processing File " & TargetFilename
        lblMessage.Refresh()
        ProcessRecords()
        'ProcessFiles(DBSource, OrderID)
        'placed here so file not moved incase of error it will wait on next file and reprocess
        If flgFailed = True Then
            'Tracing("flgFailed set to 'FALSE', exiting module")
            flgFailed = False
            Exit Sub
        End If

        'Move File
        'Tracing("Moving file " & TargetFilename)
        MoveFile(TargetFilename)
        'Now lets see if any more have come in
        'Tracing("Checking for more files")
        Dim fileEntries As String() = Directory.GetFiles(FromDir)
        Dim fileName As String
        'Tracing("Entering loop")
        For Each fileName In fileEntries
            'Tracing("In loop, file(s) found - " & fileName)
            lblMessage.Text = "Processing File " & fileName
            lblMessage.Refresh()
            'AtSymbol = InStr(fileName, "@") + 1
            'PeriodSymbol = InStr(fileName, ".")
            'DBSource = Mid(fileName, AtSymbol, PeriodSymbol - AtSymbol)
            'Tracing("Processing files in loop" & fileName)
            ProcessRecords()
            'ProcessFiles(DBSource, OrderID)
            If flgFailed = True Then Exit Sub
            TargetFilename = Mid(fileName, InStrRev(fileName, "\") + 1)
            'Tracing("Moving file " & TargetFilename)
            MoveFile(TargetFilename)
        Next fileName
        'Tracing("Loop completed, exiting normally, TextFileHandler module")
    End Sub
    'Sub ProcessFiles(ByVal Source As String, ByVal strOrderID As String)
    '    'Tracing("Entered ProcessFiles module")
    '    'we already know store that is being called from
    '    Dim CallingStore As String = Source
    '    Dim CallingStoreID As Integer
    '    Dim connRemoteDatabasestr As String = ""

    '    'Tracing("CallingStore - " & CallingStore)
    '    'get connection from databaselist array
    '    'Tracing("Geting calling store's ID")
    '    For Each Database In DatabaseList
    '        If Database.StoreName = Source Then
    '            connRemoteDatabasestr = Database.DatabaseConnection
    '            CallingStoreID = Database.StoreID
    '            Exit For
    '        End If
    '    Next
    '    If Trim(connRemoteDatabasestr) = "" Then
    '        flgFailed = True
    '        'Tracing("Could not find database connection for " & Source & " Processfile.connRemoteDatabasestr")
    '        If intconnRemoteDatabasestr_ProcessFiles >= 5 Then
    '            KillApp("Could not find database connection for " & Source & " Processfile.connRemoteDatabasestr")
    '        End If
    '        intconnRemoteDatabasestr_ProcessFiles += 1
    '        Exit Sub
    '    End If
    '    intconnRemoteDatabasestr_ProcessFiles = 0
    '    Dim connRemote As New MySqlConnection(connRemoteDatabasestr)
    '    Dim queryRemote As String
    '    'Tracing("connecting to remote database to get records to process")
    '    Try
    '        connRemote.Open()
    '    Catch
    '        connRemote.Close()
    '        Try
    '            connRemote.Open()
    '        Catch
    '            KillApp("Cannot connect with database")
    '        End Try
    '    End Try
    '    Select Case Trim(strOrderID.ToLower)
    '        Case "manual", "receivable", "adjustment", "cancel", "transfer", "recievable"
    '            queryRemote = "SELECT * FROM mverp_db_adjustments_control WHERE description = '" & Trim(strOrderID.ToLower) & "' AND processed = 'FALSE' ORDER BY created_at"
    '        Case Else
    '            queryRemote = "SELECT * FROM mverp_db_adjustments_control WHERE order_number = " & strOrderID & " AND processed = 'FALSE' ORDER BY created_at"
    '    End Select

    '    Dim cmdRemote As New MySqlCommand(queryRemote, connRemote)
    '    'do a loop here so that we can cycle through one record at a time
    '    Dim readerRemote As MySqlDataReader
    '    Dim OrderNumber As Integer = 0
    '    'Tracing("Reading database returned records for " & strOrderID)
    '    Do
    '        'reads mverp_db_adjustments in database calling function
    '        Try
    '            'Tracing("Executing reader")
    '            readerRemote = cmdRemote.ExecuteReader
    '        Catch
    '            'Tracing("Error - " & connRemoteDatabasestr)
    '            connRemote.Close()
    '            Try
    '                connRemote.Open()
    '                readerRemote = cmdRemote.ExecuteReader
    '            Catch
    '                KillApp("Cannot connect to database for " & CallingStore)
    '            End Try
    '        End Try
    '        If readerRemote.HasRows = True Then
    '            While readerRemote.Read
    '                If IsDBNull(readerRemote("order_number")) Then
    '                    lblMessage.Text = "Processsing Rec ID-" & readerRemote("id") & " SKU-" & readerRemote("sku") & " Order Number-" & readerRemote("description")
    '                    'Tracing("Processsing Rec ID-" & readerRemote("id") & " SKU-" & readerRemote("sku") & " Order Number-" & readerRemote("description"))
    '                Else
    '                    lblMessage.Text = "Processsing Rec ID-" & readerRemote("id") & " SKU-" & readerRemote("sku") & " Order Number-" & readerRemote("order_number")
    '                    'Tracing("Processsing Rec ID-" & readerRemote("id") & " SKU-" & readerRemote("sku") & " Order Number-" & readerRemote("order_number"))
    '                End If
    '                lblMessage.Refresh()
    '                ' '' ''If IsDBNull(readerRemote("order_number")) Then
    '                ' '' ''    'This is a manual change. as per meeting with jason, this will be only one line at a time
    '                ' '' ''    'Tracing("Manual Change")
    '                ' '' ''Else
    '                ' '' ''    'Tracing("Order Number - " & readerRemote("order_number"))
    '                ' '' ''End If
    '                Process_Control_Records(readerRemote, CallingStoreID, connRemoteDatabasestr, CallingStore)
    '                If flgFailed = True Then
    '                    'Tracing("Failed flag set, exiting ProcessFiles Module abnormally")
    '                    Exit Sub
    '                End If
    '            End While
    '        Else
    '            readerRemote.Close()
    '            Exit Do
    '        End If
    '        readerRemote.Close()
    '    Loop
    '    connRemote.Close()
    '    'cmdRemote.Dispose()
    '    'connRemote.Dispose()
    '    'Tracing("Exiting normally, ProcessFiles module")
    'End Sub
    Sub ProcessRecords()
        'Go Straight to source to run SP
        'first database in Databases is WVS
        'Dim strController As String = DatabaseList(0).DatabaseConnection
        Dim connController As New MySqlConnection(DBConnections("WVS"))
        'open connection
        Try
            connController.Open()
        Catch
            connController.Close()
            Try
                connController.Open()
            Catch ex As Exception
                If intSKUConnectionLoops >= 5 Then
                    SendEmail("Failed to connect to connSKURules")
                    Tracing("Failed to connect to connSKURules")
                    intSKUConnectionLoops = 0
                End If
                intSKUConnectionLoops += 1
                flgFailed = True
                connController.Close()
                connController.Dispose()
                connController.Dispose()
                Exit Sub
            End Try
        End Try
        intSKUConnectionLoops = 0

        Dim cmdController As New MySqlCommand("procMagentoControllerOrderRecords", connController)
        cmdController.CommandType = CommandType.StoredProcedure
        'cmdController.CommandText = "procMagentoControllerOrderRecords"
        cmdController.CommandTimeout = 60
        Dim readerController As MySqlDataReader
        readerController = cmdController.ExecuteReader
        Dim blnProcessPassed As Boolean = False
        Dim OrderNT As String = ""
        If readerController.HasRows = True Then
            'Process records
            While readerController.Read
                If readerController("description") = "manual" Then
                    'roll back
                    If Trim(readerController("sku")) <> "" Then
                        If readerController("source") = "WVS" Then
                            Dim OrgVal As Integer
                            Dim resultset As String = ""
                            Dim webClient As New System.Net.WebClient
                            OrgVal = GetMaster(readerController("sku"))
                            resultset = webClient.DownloadString(PHPStrings(1) & "&function=set&sku=" & readerController("sku") & "&qty=" & OrgVal)
                            ArchiveRecord(readerController("id"), readerController("source"))
                            '' '' ''    'for now if source = wvs then do not change wvs, update all others
                            '' '' ''    'make sure all other dbs match wvs
                            '' '' ''    If ProcessRecord(OrderNT, readerController("sku"), 0, readerController("source")) = True Then
                            '' '' ''        ArchiveRecord(readerController("id"), readerController("source"))
                            '' '' ''    Else
                            '' '' ''        'Mark record with process = 2 for reporting
                            '' '' ''        ArchiveRecord(readerController("id"), readerController("source"), 2)
                            '' '' ''        Exit While
                            '' '' ''    End If
                        Else
                            'roll back record
                            If RollBack(OrderNT, readerController("sku"), readerController("source"), readerController("qty")) = True Then
                                'make sure all other dbs match wvs
                                If ProcessRecord(OrderNT, readerController("sku"), 0, readerController("source")) = True Then
                                    ArchiveRecord(readerController("id"), readerController("source"))
                                Else
                                    'close and come back to Later
                                    Exit While
                                End If
                            Else
                                'close and come back to Later
                                Exit While
                            End If
                        End If
                    Else
                        ArchiveRecord(readerController("id"), readerController("source"), 3)
                    End If
                Else
                    If IsDBNull(readerController("order_number")) Then
                        OrderNT = readerController("description")
                    Else
                        OrderNT = readerController("order_number")
                    End If
                    If ProcessRecord(OrderNT, readerController("sku"), readerController("qty"), readerController("source")) = True Then
                        ArchiveRecord(readerController("id"), readerController("source"))
                    Else
                        'close and come back to Later
                        Exit While
                    End If
                End If
            End While
        End If
        readerController.Close()
        cmdController.Dispose()
        connController.Dispose()
    End Sub



    Function RollBack(ByVal OrderNoType As String, ByVal SKU As String, ByVal Source As String, ByVal New_Qty As Integer) As Boolean
        'return false for all others
        Dim ValueToSet As String = ""
        Dim resultSet As String = ""
        Dim webClient As New System.Net.WebClient
        Dim intCounter As Integer = 0

        lblMessage.Text = "Rolling Back Manual Entry  for SKU- " & SKU & " in " & Source
        lblMessage.Refresh()

        'wvs should not make it in here, but just in case
        If Source = "WVS" Then
            'get master value to reset WVS to
            Dim strQuery As String = "SELECT qty FROM `mverp_inventory_master` WHERE sku = '" & SKU & "'"
            Dim connMaster As New MySqlConnection(DBConnections("WVS"))
            connMaster.Open()
            Dim cmdmaster As New MySqlCommand(strQuery, connMaster)
            ValueToSet = cmdmaster.ExecuteScalar
            Try
                resultSet = webClient.DownloadString(PHPStrings(StoreNames(Source)) & "&function=set&sku=" & SKU & "&qty=" & ValueToSet)
                Return True
            Catch
                If InStr(resultSet, "e404") > 0 Then
                    'Clear_Rule(SKU, StoreNames(Source))
                    Tracing("Problem with Rule for SKU - " & SKU & " in store - " & Source)
                End If
                Return False
            End Try
        Else
            'everyone else
            Try
                ValueToSet = webClient.DownloadString(PHPStrings(1) & "&function=get&sku=" & SKU)
                'ValueToSet = ParseMultiWarehouseQtyOnHand(webClient.DownloadString(PHPStrings(1) & "&function=getmulti&warehouse=1&sku=" & SKU), SKU)
            Catch
                Return False
            End Try
            Try
                resultSet = webClient.DownloadString(PHPStrings(StoreNames(Source)) & "&function=set&sku=" & SKU & "&qty=" & ValueToSet)
                Return True
            Catch
                If InStr(resultSet, "e404") > 0 Then
                    'Clear_Rule(SKU, StoreNames(Source))
                    Tracing("Problem with Rule for SKU - " & SKU & " in store - " & Source)
                End If
                Return False
            End Try
        End If
        Return False
    End Function
    Function ProcessRecord(ByVal OrderNoType As String, ByVal SKU As String, ByVal New_Qty As Integer, ByVal Source As String) As Boolean
        Dim connSKURules As New MySqlConnection(DBConnections("WVS"))
        Dim webClient As New System.Net.WebClient
        Dim resultSet As String
        Dim intCounter As Integer = 0
        Dim ValueToSet As Integer = 0
        lblMessage.Text = "Processing Order No/Type - " & OrderNoType & " SKU - " & SKU
        lblMessage.Refresh()
        'open connection
        Try
            connSKURules.Open()
        Catch
            connSKURules.Close()
            Try
                connSKURules.Open()
            Catch ex As Exception
                If intSKUConnectionLoops >= 5 Then
                    SendEmail("Failed to connect to connSKURules")
                    intSKUConnectionLoops = 0
                End If
                intSKUConnectionLoops += 1
                flgFailed = True
                connSKURules.Close()
                connSKURules.Dispose()
                Return False
            End Try
        End Try
        intSKUConnectionLoops = 0
        Dim RuleCount As Integer = 0
        Dim querySKURules As String = "SELECT COUNT(*) FROM mverp_db_adjustments_skus_rules WHERE sku = '" & SKU & "'"
        Dim cmdSKURules As New MySqlCommand(querySKURules, connSKURules)
        RuleCount = cmdSKURules.ExecuteScalar
        If RuleCount = 0 Then
            'no sku rules to process
            connSKURules.Dispose()
            webClient.Dispose()
            Return True
        End If
        'Get main value from WVS to use for all
        Dim Value_To_Set As String = ""
        Try
            Value_To_Set = webClient.DownloadString(PHPStrings(1) & "&function=get&sku=" & SKU)
            'Value_To_Set = ParseMultiWarehouseQtyOnHand(webClient.DownloadString(PHPStrings(1) & "&function=getmulti&warehouse=1&sku=" & SKU), SKU)
            'UpdateMaster(CInt(Value_To_Set), SKU)
        Catch
            If Value_To_Set = "e404" Then
                Return False
            ElseIf Value_To_Set = "" Then
                Return False
            End If
        End Try
        If Source = "WVS" Then ' do not reprocess wvs items already set in magento
            resultSet = webClient.DownloadString(PHPStrings(1) & "&function=set&sku=" & SKU & "&qty=" & Value_To_Set)
            UpdateMaster(CInt(Value_To_Set), SKU)
        Else
            'do calculations on value_to_set
            Value_To_Set = Value_To_Set + New_Qty
            'update wvs with new value
            Try
                resultSet = webClient.DownloadString(PHPStrings(1) & "&function=set&sku=" & SKU & "&qty=" & Value_To_Set)
                UpdateMaster(CInt(Value_To_Set), SKU)
                'resultSet = ParseMultiWarehouseQtyOnHand(webClient.DownloadString(PHPStrings(1) & "&function=setmulti&warehouse=1&sku=" & SKU & "&qty=" & Value_To_Set), SKU)
            Catch
                If Value_To_Set = "e404" Or Value_To_Set = "Failed Call" Then
                    Tracing(Value_To_Set & " - " & PHPStrings(1) & "&function=set&sku=" & SKU & "&qty=" & Value_To_Set)
                    'Tracing(Value_To_Set & " - " & PHPStrings(1) & "&function=setmulti&warehouse=1&sku=" & SKU & "&qty=" & Value_To_Set)
                    Return False
                End If
            End Try
        End If
        'do not set others to less than 0
        If Trim(Value_To_Set) = "" Then Value_To_Set = 0
        If Value_To_Set < 0 Then Value_To_Set = 0
        cmdSKURules.CommandText = "SELECT * FROM mverp_db_adjustments_skus_rules WHERE sku = '" & SKU & "'"
        Dim readerSKURules As MySqlDataReader
        readerSKURules = cmdSKURules.ExecuteReader
        If readerSKURules.HasRows Then
            While readerSKURules.Read
                Select Case readerSKURules("rule_id")
                    Case 1 'Do Nothing for now
                    Case 2 'Do Nothing for now
                    Case 3 'Both
                        lblMessage.Text = "Processing Order No/Type - " & OrderNoType & " SKU - " & SKU & " in " & StoreIDs(readerSKURules("store_id")) & " to new value of - " & Value_To_Set
                        lblMessage.Refresh()
                        Try
                            resultSet = webClient.DownloadString(PHPStrings(readerSKURules("store_id")) & "&function=set&sku=" & SKU & "&qty=" & Value_To_Set)
                        Catch
                            Tracing("Failed to update sku - " & SKU & " in " & StoreNames(readerSKURules("store_id")))
                            flgFailed = False
                        End Try

                        Mark_As_Synced(readerSKURules("id"))
                    Case 4 'Do Nothing for now
                    Case Else
                        'Do Nothing
                End Select
            End While
        End If
        connSKURules.Dispose()
        webClient.Dispose()
        Return True
    End Function

    'New 2015-09-16
    Sub ArchiveRecord(ByVal RecordID As Integer, ByVal Source As String, Optional ByVal Processed As Integer = 1)
        Dim connArchiveUpdate As New MySqlConnection(DBConnections(Source))
        Dim queryArchiveUpdate As String = "UPDATE  mverp_db_adjustments_control set processed = " & Processed & ", processed_datetime ='" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "' WHERE id = " & RecordID

        Try
            connArchiveUpdate.Open()
            'Tracing("Archiving Record - " & Rec_ID)
            Dim cmdArchiveUpdate As New MySqlCommand(queryArchiveUpdate, connArchiveUpdate)
            cmdArchiveUpdate.ExecuteScalar()
            cmdArchiveUpdate.Dispose()
        Catch ex As Exception
            SendEmail("Error trying to archive Rec_id - " & RecordID & " for " & Source & ", exiting module")
            flgFailed = True
        Finally
            connArchiveUpdate.Close()
            connArchiveUpdate.Dispose()
        End Try
    End Sub
    Sub Mark_As_Synced(ByVal Sync_ID As Integer)
        Dim connSync As New MySqlConnection(DBConnections("WVS"))
        Dim querySyncDate As String = "UPDATE mverp_db_adjustments_skus_rules SET last_synced = '" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "' WHERE id = " & Sync_ID
        Try
            connSync.Open()
            'Tracing("Archiving Record - " & Rec_ID)
            Dim cmdArchiveUpdate As New MySqlCommand(querySyncDate, connSync)
            cmdArchiveUpdate.ExecuteScalar()
            cmdArchiveUpdate.Dispose()
        Catch ex As Exception
            'SendEmail("Error trying to archive Rec_id - " & RecordID & " for " & Source & ", exiting module")
            flgFailed = True
        Finally
            connSync.Close()
            connSync.Dispose()
        End Try
    End Sub
    '' '' ''Sub Process_Control_Records(ByRef RecordSetRow As MySqlDataReader, ByVal CallingStoreID As Integer, ByVal connRemoteDatabasestr As String, ByRef CallingStore As String)
    '' '' ''    'Tracing("Entered Process_Control_Records module")
    '' '' ''    'simplified and straight forward
    '' '' ''    'first check to see if calling store is wvs
    '' '' ''    Dim MasterStore As String
    '' '' ''    Dim MasterStorePHP As String = ""
    '' '' ''    Dim connSKURules As New MySqlConnection(connstr)
    '' '' ''    Dim querySKURules As String = "SELECT * FROM mverp_db_adjustments_skus_rules WHERE sku = '" & RecordSetRow("SKU") & "'"
    '' '' ''    Dim cmdSKURules As New MySqlCommand(querySKURules, connSKURules)
    '' '' ''    Dim readerSKURules As MySqlDataReader
    '' '' ''    Dim RemotePHP As String = ""
    '' '' ''    Dim ValueToSet As String = ""
    '' '' ''    Dim intCounter As Integer = 0
    '' '' ''    Dim webClient As New System.Net.WebClient
    '' '' ''    Dim resultSet As String
    '' '' ''    'Dim resultGet As String
    '' '' ''    Dim intNumberofSkuRules As Integer
    '' '' ''    Dim querySKURulesCount As String = "SELECT Count(*) FROM mverp_db_adjustments_skus_rules WHERE sku = '" & RecordSetRow("SKU") & "'"
    '' '' ''    Dim querySKURulesExclude As String = "SELECT * FROM mverp_db_adjustments_skus_rules WHERE sku = '" & RecordSetRow("SKU") & "' AND store_id <> " & CallingStoreID
    '' '' ''    Dim querySKURulesCallingStoreOnly As String = "SELECT * FROM mverp_db_adjustments_skus_rules WHERE sku = '" & RecordSetRow("SKU") & "' AND store_id = " & CallingStoreID


    '' '' ''    'open connection
    '' '' ''    Try
    '' '' ''        connSKURules.Open()
    '' '' ''    Catch
    '' '' ''        connSKURules.Close()
    '' '' ''        Try
    '' '' ''            connSKURules.Open()
    '' '' ''        Catch ex As Exception
    '' '' ''            If intSKUConnectionLoops >= 5 Then
    '' '' ''                SendEmail("Failed to connect to connSKURules")
    '' '' ''                intSKUConnectionLoops = 0
    '' '' ''            End If
    '' '' ''            intSKUConnectionLoops += 1
    '' '' ''            flgFailed = True
    '' '' ''            connSKURules.Close()
    '' '' ''            connSKURules.Dispose()
    '' '' ''            cmdSKURules.Dispose()
    '' '' ''            webClient.Dispose()
    '' '' ''            Exit Sub
    '' '' ''        End Try
    '' '' ''    End Try
    '' '' ''    intSKUConnectionLoops = 0
    '' '' ''    'Tracing("Getting Master Store information")
    '' '' ''    For Each Database In DatabaseList
    '' '' ''        If Database.StoreName = "WVS" Then
    '' '' ''            MasterStore = "WVS"
    '' '' ''            MasterStorePHP = Database.PHPWebsite
    '' '' ''            Exit For
    '' '' ''        End If
    '' '' ''    Next

    '' '' ''    'Tracing("Calling Store is " & CallingStore)
    '' '' ''    If CallingStore = "WVS" Then
    '' '' ''        'can only go one direction from master to remote
    '' '' ''        'open recordset
    '' '' ''        Try
    '' '' ''            'Tracing("Reading SKU rules read")
    '' '' ''            readerSKURules = cmdSKURules.ExecuteReader
    '' '' ''        Catch ex As Exception
    '' '' ''            'Tracing("Error executing rules read, exiting module")
    '' '' ''            SendEmail("Magento Comparator Error - Trying to read SKU rules - " & ex.Message)
    '' '' ''            flgFailed = True
    '' '' ''            readerSKURules.Close()
    '' '' ''            connSKURules.Close()
    '' '' ''            connSKURules.Dispose()
    '' '' ''            cmdSKURules.Dispose()
    '' '' ''            webClient.Dispose()
    '' '' ''            'jump out of sub and do not archive  record
    '' '' ''            Exit Sub
    '' '' ''        End Try

    '' '' ''        If readerSKURules.HasRows = True Then
    '' '' ''            While readerSKURules.Read
    '' '' ''                Try
    '' '' ''                    If IsDBNull(readerSKURules("rule_id")) = True Then
    '' '' ''                        'Tracing("Has No Rules for SKU " & RecordSetRow("SKU") & ", archiving")
    '' '' ''                        'No Rule Found Just continue
    '' '' ''                        Add_To_Archive(RecordSetRow("ID"), connRemoteDatabasestr, CallingStore)
    '' '' ''                    Else 'Rules found
    '' '' ''                        'Tracing("Has Rules")
    '' '' ''                        Select Case readerSKURules("rule_id")
    '' '' ''                            Case 1 'Do Nothing for now
    '' '' ''                            Case 2 'Do Nothing for now
    '' '' ''                            Case 3 'Both
    '' '' ''                                'get remote store id and php info
    '' '' ''                                For Each Database In DatabaseList
    '' '' ''                                    If Database.StoreID = readerSKURules("store_id") Then
    '' '' ''                                        RemotePHP = Database.PHPWebsite
    '' '' ''                                        Exit For
    '' '' ''                                    End If
    '' '' ''                                Next
    '' '' ''                                'going from wvs to remote
    '' '' ''                                'value in current_qty is not corrected so add to qty
    '' '' ''                                'If RecordSetRow("description") = "Receivable" Or RecordSetRow("description") = "Adjustment" Or RecordSetRow("description") = "order" Then
    '' '' ''                                '    ValueToSet = RecordSetRow("current_qty") + RecordSetRow("qty")
    '' '' ''                                'Else
    '' '' ''                                '    ValueToSet = RecordSetRow("current_qty")
    '' '' ''                                'End If
    '' '' ''                                ValueToSet = RecordSetRow("current_qty")

    '' '' ''                                If ValueToSet <= 0 Then
    '' '' ''                                    'put into remote database
    '' '' ''                                    resultSet = webClient.DownloadString(RemotePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=0")
    '' '' ''                                    'Tracing(RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=0 - Result = " & resultSet)
    '' '' ''                                    flgFailed = False
    '' '' ''                                    intCounter = 1
    '' '' ''                                    If InStr(resultSet, "e404") > 0 Then
    '' '' ''                                        Do Until InStr(resultSet, "e404") = 0
    '' '' ''                                            resultSet = webClient.DownloadString(RemotePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=0")
    '' '' ''                                            'Tracing("Loop # " & intCounter & " " & RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=0 - Result = " & resultSet)
    '' '' ''                                            intCounter += 1
    '' '' ''                                            If intCounter >= 10 Then
    '' '' ''                                                SendEmail("Error trying to contact web page - " & RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=0 - Result = " & resultSet)
    '' '' ''                                                Add_To_Archive(RecordSetRow("ID"), connRemoteDatabasestr, CallingStore, 2)
    '' '' ''                                                flgFailed = True
    '' '' ''                                                Exit Sub
    '' '' ''                                            End If
    '' '' ''                                        Loop
    '' '' ''                                    End If
    '' '' ''                                    'reset resultget = 0 at this point, because we do not set other then WVS to less than 0
    '' '' ''                                    ValueToSet = 0
    '' '' ''                                    intCounter = 1
    '' '' ''                                    Do Until ValueToSet = resultSet
    '' '' ''                                        resultSet = webClient.DownloadString(RemotePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=0")
    '' '' ''                                        'Tracing("Loop #" & intCounter & " - " & RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=0 - Result = " & resultSet)
    '' '' ''                                        If intCounter >= 5 Then
    '' '' ''                                            'fail the operation
    '' '' ''                                            'Tracing("Could not update SKU - " & RecordSetRow("SKU") & " to qty=" & ValueToSet & " in " & CallingStore & " database, exiting module")
    '' '' ''                                            SendEmail("Magento Comparator Error - Could not update SKU - " & RecordSetRow("SKU") & " to qty=" & ValueToSet & " in " & CallingStore & " database")
    '' '' ''                                            flgFailed = True
    '' '' ''                                            Add_To_Archive(RecordSetRow("ID"), connRemoteDatabasestr, CallingStore, 2)
    '' '' ''                                            readerSKURules.Close()
    '' '' ''                                            connSKURules.Close()
    '' '' ''                                            connSKURules.Dispose()
    '' '' ''                                            cmdSKURules.Dispose()
    '' '' ''                                            webClient.Dispose()
    '' '' ''                                            'jump out of sub and do not archive  record
    '' '' ''                                            Exit Sub
    '' '' ''                                        End If
    '' '' ''                                        intCounter += 1
    '' '' ''                                    Loop
    '' '' ''                                Else
    '' '' ''                                    'Put enitire value into remote stores database
    '' '' ''                                    resultSet = webClient.DownloadString(RemotePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet)
    '' '' ''                                    'Tracing(RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet & " - Resultset " & resultSet)
    '' '' ''                                    flgFailed = False
    '' '' ''                                    intCounter = 1
    '' '' ''                                    If InStr(resultSet, "e404") > 0 Then
    '' '' ''                                        Do Until InStr(resultSet, "e404") = 0
    '' '' ''                                            resultSet = webClient.DownloadString(RemotePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=0")
    '' '' ''                                            'Tracing("Loop # " & intCounter & " " & RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=0 - Result = " & resultSet)
    '' '' ''                                            intCounter += 1
    '' '' ''                                            If intCounter >= 10 Then
    '' '' ''                                                SendEmail("Error trying to contact web page - " & RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=0 - Result = " & resultSet)
    '' '' ''                                                Add_To_Archive(RecordSetRow("ID"), connRemoteDatabasestr, CallingStore, 2)
    '' '' ''                                                flgFailed = True
    '' '' ''                                                connSKURules.Dispose()
    '' '' ''                                                cmdSKURules.Dispose()
    '' '' ''                                                webClient.Dispose()
    '' '' ''                                                Exit Sub
    '' '' ''                                            End If
    '' '' ''                                        Loop
    '' '' ''                                    End If
    '' '' ''                                    intCounter = 1
    '' '' ''                                    Do Until ValueToSet = resultSet
    '' '' ''                                        resultSet = webClient.DownloadString(RemotePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet)
    '' '' ''                                        'Tracing("Loop #" & intCounter & " - " & RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet & " - Result = " & resultSet)
    '' '' ''                                        If intCounter >= 5 Then
    '' '' ''                                            'fail the operation
    '' '' ''                                            'Tracing("Could not update SKU - " & RecordSetRow("SKU") & " to qty=" & ValueToSet & " in " & CallingStore & " database")
    '' '' ''                                            SendEmail("Magento Comparator Error - Could not update SKU - " & RecordSetRow("SKU") & " to qty=" & ValueToSet & " in " & CallingStore & " database")
    '' '' ''                                            flgFailed = True
    '' '' ''                                            Add_To_Archive(RecordSetRow("ID"), connRemoteDatabasestr, CallingStore, 2)
    '' '' ''                                            readerSKURules.Close()
    '' '' ''                                            connSKURules.Close()
    '' '' ''                                            'jump out of sub and do not archive  record
    '' '' ''                                            connSKURules.Dispose()
    '' '' ''                                            cmdSKURules.Dispose()
    '' '' ''                                            webClient.Dispose()
    '' '' ''                                            Exit Sub
    '' '' ''                                        End If
    '' '' ''                                        intCounter += 1
    '' '' ''                                    Loop
    '' '' ''                                End If
    '' '' ''                            Case 4 'Do Nothing for now
    '' '' ''                            Case Else
    '' '' ''                                'Do Nothing
    '' '' ''                        End Select
    '' '' ''                    End If
    '' '' ''                Catch ev As Exception
    '' '' ''                    'Tracing("Error - " & ev.Message & ", exiting module")
    '' '' ''                    SendEmail("Magento Comparator Error - " & ev.Message)
    '' '' ''                    flgFailed = True
    '' '' ''                    readerSKURules.Close()
    '' '' ''                    connSKURules.Close()
    '' '' ''                    connSKURules.Dispose()
    '' '' ''                    cmdSKURules.Dispose()
    '' '' ''                    webClient.Dispose()
    '' '' ''                    'jump out of sub and do not archive  record
    '' '' ''                    Exit Sub
    '' '' ''                End Try
    '' '' ''            End While
    '' '' ''            'make sure wvs have correct value
    '' '' ''        End If
    '' '' ''    Else 'All other stores
    '' '' ''        'start by seeing if more than one sku rule exists
    '' '' ''        'if it does then process calling store first to send updated value to wvs first
    '' '' ''        'then call remaining sku rules
    '' '' ''        cmdSKURules.CommandText = querySKURulesCount
    '' '' ''        intNumberofSkuRules = cmdSKURules.ExecuteScalar
    '' '' ''        ValueToSet = RecordSetRow("current_qty")
    '' '' ''        If intNumberofSkuRules > 0 Then
    '' '' ''            'process wvs first
    '' '' ''            'get one sku rule for calling store
    '' '' ''            'Tracing("Getting remote PHP information")
    '' '' ''            For Each Database In DatabaseList
    '' '' ''                If Database.StoreID = CallingStoreID Then
    '' '' ''                    RemotePHP = Database.PHPWebsite
    '' '' ''                    Exit For
    '' '' ''                End If
    '' '' ''            Next
    '' '' ''            'not advanced inventory so value in current qty is corrected value
    '' '' ''            'Tracing("Setting WVS First")
    '' '' ''            intCounter = 1
    '' '' ''            resultSet = ParseMultiWarehouseQtyOnHand(webClient.DownloadString(MasterStorePHP & "&function=setmulti&warehouse=1&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet))
    '' '' ''            'resultSet = webClient.DownloadString(MasterStorePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet)
    '' '' ''            'Tracing(MasterStorePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet & " Results-" & resultSet)
    '' '' ''            If resultSet = "Failed Call" Then
    '' '' ''                'Tracing("Failed Call - " & MasterStorePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet)
    '' '' ''                SendEmail("Record ID #" & RecordSetRow("ID") & "  Order #" & RecordSetRow("order_number") & "  SKU-" & RecordSetRow("sku") & " can not be processes. Setting Processed to '3'. Please investigate.")
    '' '' ''                Add_To_Archive(RecordSetRow("ID"), connRemoteDatabasestr, CallingStore, 3)
    '' '' ''                'Tracing("Exiting abnormally Process_Control_Records module, email sent")
    '' '' ''                connSKURules.Dispose()
    '' '' ''                cmdSKURules.Dispose()
    '' '' ''                webClient.Dispose()
    '' '' ''                Exit Sub
    '' '' ''            End If

    '' '' ''            flgFailed = False
    '' '' ''            intCounter = 1
    '' '' ''            If InStr(resultSet, "e404") > 0 Then
    '' '' ''                Do Until InStr(resultSet, "404") = 0
    '' '' ''                    resultSet = ParseMultiWarehouseQtyOnHand(webClient.DownloadString(MasterStorePHP & "&function=setmulti&warehouse=1&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet))
    '' '' ''                    'resultSet = webClient.DownloadString(MasterStorePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet)
    '' '' ''                    'Tracing("Loop # " & intCounter & " " & MasterStorePHP & "&function=set&sku=" & RecordSetRow("SKU") & " Results-" & resultSet)
    '' '' ''                    intCounter += 1
    '' '' ''                    If intCounter >= 10 Then
    '' '' ''                        SendEmail("Error trying to contact web page - " & MasterStorePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet & " Results-" & resultSet)
    '' '' ''                        flgFailed = True
    '' '' ''                        Add_To_Archive(RecordSetRow("ID"), connRemoteDatabasestr, CallingStore, 2)
    '' '' ''                        connSKURules.Dispose()
    '' '' ''                        cmdSKURules.Dispose()
    '' '' ''                        webClient.Dispose()
    '' '' ''                        Exit Sub
    '' '' ''                    End If
    '' '' ''                Loop
    '' '' ''            End If
    '' '' ''            Do Until ValueToSet = resultSet
    '' '' ''                resultSet = ParseMultiWarehouseQtyOnHand(webClient.DownloadString(MasterStorePHP & "&function=setmulti&warehouse=1&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet))
    '' '' ''                'Tracing("Loop #" & intCounter & " - " & MasterStorePHP & "?function=setmulti&warehouse=1&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet & " - ResultSet = " & resultSet)
    '' '' ''                If intCounter >= 5 Then
    '' '' ''                    'Failed to update
    '' '' ''                    flgFailed = True
    '' '' ''                    readerSKURules.Close()
    '' '' ''                    connSKURules.Close()
    '' '' ''                    connSKURules.Dispose()
    '' '' ''                    cmdSKURules.Dispose()
    '' '' ''                    webClient.Dispose()
    '' '' ''                    'Tracing("Could not update SKU - " & RecordSetRow("SKU") & " to qty=" & ValueToSet & " in remote database, exiting module")
    '' '' ''                    SendEmail("Magento Comparator Error - Could not update SKU - " & RecordSetRow("SKU") & " to qty=" & ValueToSet & " in remote database")
    '' '' ''                    'jump out of sub and do not archive  record
    '' '' ''                    Exit Sub
    '' '' ''                End If
    '' '' ''                intCounter += 1
    '' '' ''            Loop
    '' '' ''            'finally call again to finish processing sku rules excluding calling store's id
    '' '' ''            cmdSKURules.CommandText = querySKURulesExclude

    '' '' ''            ''for remaining sku rules
    '' '' ''            Try
    '' '' ''                readerSKURules = cmdSKURules.ExecuteReader
    '' '' ''            Catch ex As Exception
    '' '' ''                flgFailed = True
    '' '' ''                readerSKURules.Close()
    '' '' ''                connSKURules.Close()
    '' '' ''                connSKURules.Dispose()
    '' '' ''                cmdSKURules.Dispose()
    '' '' ''                webClient.Dispose()
    '' '' ''                'Tracing("Could not update SKU - " & RecordSetRow("SKU") & " to qty=" & ValueToSet & " in remote database, exiting module")
    '' '' ''                SendEmail("Magento Comparator Error - Could not read SKU rules")
    '' '' ''                'jump out of sub and do not archive  record
    '' '' ''                Exit Sub
    '' '' ''            End Try
    '' '' ''            While readerSKURules.Read
    '' '' ''                For Each Database In DatabaseList
    '' '' ''                    If Database.StoreID = readerSKURules("store_id") Then
    '' '' ''                        RemotePHP = Database.PHPWebsite
    '' '' ''                        Exit For
    '' '' ''                    End If
    '' '' ''                Next
    '' '' ''                'corrected qty already in wvs
    '' '' ''                'Tracing("Set other stores to new value")
    '' '' ''                resultSet = webClient.DownloadString(RemotePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet)
    '' '' ''                'Tracing(RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet & " - Result = " & resultSet)
    '' '' ''                flgFailed = False
    '' '' ''                intCounter = 1
    '' '' ''                If InStr(resultSet, "e404") > 0 Then
    '' '' ''                    Do Until InStr(resultSet, "e404") = 0
    '' '' ''                        resultSet = webClient.DownloadString(RemotePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=0")
    '' '' ''                        'Tracing("Loop # " & intCounter & " " & RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=0 - Result = " & resultSet)
    '' '' ''                        intCounter += 1
    '' '' ''                        If intCounter >= 10 Then
    '' '' ''                            SendEmail("Error trying to contact web page - " & RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=0 - Result = " & resultSet)
    '' '' ''                            flgFailed = True
    '' '' ''                            Add_To_Archive(RecordSetRow("ID"), connRemoteDatabasestr, CallingStore, 2)
    '' '' ''                            connSKURules.Dispose()
    '' '' ''                            cmdSKURules.Dispose()
    '' '' ''                            webClient.Dispose()
    '' '' ''                            Exit Sub
    '' '' ''                        End If
    '' '' ''                    Loop
    '' '' ''                End If
    '' '' ''                intCounter = 1
    '' '' ''                Do Until ValueToSet = resultSet
    '' '' ''                    resultSet = webClient.DownloadString(RemotePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet)
    '' '' ''                    'Tracing("Loop #" & intCounter & " - " & RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet & " - Result = " & resultSet)
    '' '' ''                    If intCounter > -5 Then
    '' '' ''                        'Failed to update
    '' '' ''                        flgFailed = True
    '' '' ''                        readerSKURules.Close()
    '' '' ''                        connSKURules.Close()
    '' '' ''                        connSKURules.Dispose()
    '' '' ''                        cmdSKURules.Dispose()
    '' '' ''                        webClient.Dispose()
    '' '' ''                        'Tracing("Could not update SKU - " & RecordSetRow("SKU") & " to qty=" & ValueToSet & " in remote database, exiting module")
    '' '' ''                        SendEmail("Magento Comparator Error - Could not update SKU - " & RecordSetRow("SKU") & " to qty=" & ValueToSet & " in remote database")
    '' '' ''                        'jump out of sub and do not archive  record
    '' '' ''                        Exit Sub
    '' '' ''                    End If
    '' '' ''                    intCounter += 1
    '' '' ''                Loop
    '' '' ''            End While

    '' '' ''            'final step, make sure calling store has same value as recordset
    '' '' ''            'Tracing("Making sure calling store has correct value in it")
    '' '' ''            For Each Database In DatabaseList
    '' '' ''                If Database.StoreID = CallingStoreID Then
    '' '' ''                    RemotePHP = Database.PHPWebsite
    '' '' ''                    Exit For
    '' '' ''                End If
    '' '' ''            Next
    '' '' ''            resultSet = webClient.DownloadString(RemotePHP & "&function=get&sku=" & RecordSetRow("SKU"))
    '' '' ''            intCounter = 1
    '' '' ''            Do Until ValueToSet = resultSet
    '' '' ''                resultSet = webClient.DownloadString(RemotePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet)
    '' '' ''                'Tracing("Loop #" & intCounter & " - " & RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=" & ValueToSet & " - Result = " & resultSet)
    '' '' ''                flgFailed = False
    '' '' ''                intCounter = 1
    '' '' ''                If InStr(resultSet, "e404") > 0 Then
    '' '' ''                    Do Until InStr(resultSet, "e404") = 0
    '' '' ''                        resultSet = webClient.DownloadString(RemotePHP & "&function=set&sku=" & RecordSetRow("SKU") & "&qty=0")
    '' '' ''                        'Tracing("Loop # " & intCounter & " " & RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=0 - Result = " & resultSet)
    '' '' ''                        intCounter += 1
    '' '' ''                        If intCounter >= 10 Then
    '' '' ''                            SendEmail("Error trying to contact web page - " & RemotePHP & "?function=set&sku=" & RecordSetRow("SKU") & "&qty=0 - Result = " & resultSet)
    '' '' ''                            flgFailed = True
    '' '' ''                            Add_To_Archive(RecordSetRow("ID"), connRemoteDatabasestr, CallingStore, 2)
    '' '' ''                            connSKURules.Dispose()
    '' '' ''                            cmdSKURules.Dispose()
    '' '' ''                            webClient.Dispose()
    '' '' ''                            Exit Sub
    '' '' ''                        End If
    '' '' ''                    Loop
    '' '' ''                End If
    '' '' ''                If intCounter >= 5 Then
    '' '' ''                    'fail the operation
    '' '' ''                    'Tracing("Could not update SKU - " & RecordSetRow("SKU") & " to qty=" & ValueToSet & " in " & CallingStore & " database, exiting module")
    '' '' ''                    SendEmail("Magento Comparator Error - Could not update SKU - " & RecordSetRow("SKU") & " to qty=" & ValueToSet & " in " & CallingStore & " database")
    '' '' ''                    flgFailed = True
    '' '' ''                    readerSKURules.Close()
    '' '' ''                    connSKURules.Close()
    '' '' ''                    connSKURules.Dispose()
    '' '' ''                    cmdSKURules.Dispose()
    '' '' ''                    webClient.Dispose()
    '' '' ''                    'jump out of sub and do not archive record
    '' '' ''                    Exit Sub
    '' '' ''                End If
    '' '' ''            Loop
    '' '' ''            readerSKURules.Close()
    '' '' ''            connSKURules.Close()
    '' '' ''        End If
    '' '' ''    End If
    '' '' ''    Add_To_Archive(RecordSetRow("ID"), connRemoteDatabasestr, CallingStore)
    '' '' ''    connSKURules.Dispose()
    '' '' ''    cmdSKURules.Dispose()
    '' '' ''    webClient.Dispose()

    '' '' ''    'readerSKURules.Close()
    '' '' ''    'connSKURules.Close()
    '' '' ''    'Tracing("Exiting normally, Process_Control_Records module")
    '' '' ''End Sub
    '' '' ''Sub Add_To_Archive(ByVal Rec_ID As Integer, ByVal connRemoteDatabasestr As String, ByVal CallingStore As String, Optional ByVal Processed As Integer = 1)
    '' '' ''    Dim connArchiveUpdate As New MySqlConnection(connRemoteDatabasestr)
    '' '' ''    Dim queryArchiveUpdate As String

    '' '' ''    Try
    '' '' ''        connArchiveUpdate.Open()
    '' '' ''        'Tracing("Archiving Record - " & Rec_ID)
    '' '' ''        queryArchiveUpdate = "UPDATE  mverp_db_adjustments_control set processed = " & Processed & ", processed_datetime ='" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "' "
    '' '' ''        queryArchiveUpdate += " WHERE id = " & Rec_ID
    '' '' ''        Dim cmdArchiveUpdate As New MySqlCommand(queryArchiveUpdate, connArchiveUpdate)
    '' '' ''        cmdArchiveUpdate.ExecuteScalar()
    '' '' ''        cmdArchiveUpdate.Dispose()
    '' '' ''    Catch ex As Exception
    '' '' ''        SendEmail("Error trying to archive Rec_id - " & Rec_ID & ", exiting module")
    '' '' ''        flgFailed = True
    '' '' ''    Finally
    '' '' ''        connArchiveUpdate.Close()
    '' '' ''        connArchiveUpdate.Dispose()
    '' '' ''    End Try
    '' '' ''End Sub
    '' '' ''Function ParseMultiWarehouseQtyOnHand(ByVal WebString As String, ByVal inSKU As String) As String
    '' '' ''    Dim webClient As New System.Net.WebClient
    '' '' ''    Dim response As String = ""
    '' '' ''    'response = webClient.DownloadString(WebString)
    '' '' ''    response = WebString
    '' '' ''    Dim reader As New XmlTextReader(New StringReader(response))
    '' '' ''    Dim xmldoc As New XmlDocument()
    '' '' ''    Dim xmlnode As XmlNodeList
    '' '' ''    Try
    '' '' ''        xmldoc.Load(reader)
    '' '' ''    Catch
    '' '' ''        'MsgBox(Err.Description & " - " & Err.Number)
    '' '' ''        ParseMultiWarehouseQtyOnHand = "Failed Call"
    '' '' ''        Exit Function
    '' '' ''    End Try
    '' '' ''    'Error routine
    '' '' ''    xmlnode = xmldoc.GetElementsByTagName("magento")

    '' '' ''    If xmlnode.Count <> 0 Then
    '' '' ''        ParseMultiWarehouseQtyOnHand = xmlnode.Item(0).ChildNodes.Item(0).InnerText
    '' '' ''        'SendEmail("Multi WareHouse Not Set - " & inSKU)
    '' '' ''        Exit Function
    '' '' ''    End If
    '' '' ''    'get warehouse 1 node info
    '' '' ''    xmlnode = xmldoc.GetElementsByTagName("warehouses")
    '' '' ''    ParseMultiWarehouseQtyOnHand = xmlnode.Item(0).ChildNodes.Item(2).InnerText
    '' '' ''    webClient.Dispose()
    '' '' ''    reader.Dispose()
    '' '' ''    xmldoc = Nothing
    '' '' ''End Function
    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        Dim fileEntries As String() = Directory.GetFiles(FromDir)
        Dim fileName As String
        'Dim fileNames() As String

        Timer1.Stop()
        SyncTimer.Stop()
        'fileEntries.Count()
        Array.Sort(fileEntries)
        FileMonitor.EnableRaisingEvents = False
        lblMessage.Text = "Manually Started"
        lblMessage.Refresh()
        'Tracing("Manually Started")
        For Each fileName In fileEntries
            TextFileHandler(fileName)
            'Debug.Print(fileName)
        Next
        'FileMonitor.EnableRaisingEvents = False
        'FileMonitor.EnableRaisingEvents = True
        lblMessage.Text = "Restarted at " & Now.ToString
        lblMessage.Refresh()
        fileEntries = Nothing
        FileMonitor.EnableRaisingEvents = True
        Timer1.Start()
        SyncTimer.Start()
    End Sub
    Sub MoveFile(ByVal TargetFileName As String)
        Try
            File.Move(FromDir & "\" & TargetFileName, ToDir & "\" & TargetFileName)
            'Tracing("Moving file " & TargetFileName)
        Catch ex As Exception
            'Tracing("error moving - " & ex.Message)
            Try
                File.Delete(FromDir & "\" & TargetFileName)
            Catch er As Exception
                'Tracing("error deleteing- " & er.Message)
            End Try
        End Try
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        btnStart_Click(Me, e)
    End Sub
    Private Sub KillApp(ByVal EmailMessage As String)
        SendEmail(EmailMessage)
        'Tracing(EmailMessage)
        CreateRestartFile()
        'Tracing("******************Restarting Program************************************************")
        Process.Start(Application.StartupPath.ToString() & "\MagentoDataControllerLiveII.exe")
        Application.Exit()
        End
    End Sub
    Private Sub Orphans()
        Dim query = "SELECT * FROM mverp_db_adjustments_control WHERE processed = FALSE"
        Dim connOrphan As New MySqlConnection(connstr)
        Dim cmdOrphan As New MySqlCommand(query, connOrphan)
        Dim reader As MySqlDataReader
        Dim webClient As New System.Net.WebClient
        Dim response As String = ""
        Dim strProcess As String
        connOrphan.Open()
        reader = cmdOrphan.ExecuteReader
        While reader.Read
            If IsDBNull(reader("order_number")) = False Then
                strProcess = reader("order_number")
            Else
                strProcess = reader("description")
            End If

            If InStr(connstr, "test") > 0 Then
                response = webClient.DownloadString("http://sync.wholesalevapingsupply.com/process.aspx?source=" & reader("source") & "&ver=dev&orderid=" & strProcess)
            Else
                response = webClient.DownloadString("http://sync.wholesalevapingsupply.com/process.aspx?source=" & reader("source") & "&ver=live&orderid=" & strProcess)
            End If
        End While
        reader.Close()
        connOrphan.Close()
        connOrphan.Dispose()
        cmdOrphan.Dispose()
        webClient.Dispose()
    End Sub

    Private Sub SyncTimer_Tick(sender As Object, e As EventArgs) Handles SyncTimer.Tick
        Timer1.Stop()
        SyncTimer.Stop()
        SyncRules()
        SyncTimer.Start()
        Timer1.Start()
    End Sub
    Public Sub SyncRules()
        Dim querySync As String = "SELECT sku FROM mverp_db_adjustments_skus_rules WHERE (last_synced < DATE_SUB('" & Format(Now, "yyyy/MM/dd HH:mm:ss") & "',INTERVAL 24 HOUR) OR last_synced IS NULL) and rule_id = 3 GROUP BY sku LIMIT 1;"
        Dim connSync As New MySqlConnection(DBConnections("WVS"))
        Try
            connSync.Open()
        Catch
            connSync.Close()
            Try
                connSync.Open()
            Catch ex As Exception
                If intSKUConnectionLoops >= 5 Then
                    SendEmail("Failed to connect to connSync")
                    intSKUConnectionLoops = 0
                End If
                intSKUConnectionLoops += 1
                flgFailed = True
                connSync.Close()
                connSync.Dispose()
            End Try
        End Try
        Dim strReturnedSku As String = ""
        Dim cmdsync As New MySqlCommand(querySync, connSync)
        strReturnedSku = cmdsync.ExecuteScalar

        Dim Value_To_Set As String = GetMaster(strReturnedSku)

        Dim webClient As New System.Net.WebClient

        ' '' ''Try
        ' '' ''    Value_To_Set = webClient.DownloadString(PHPStrings(1) & "&function=get&sku=" & strReturnedSku)
        ' '' ''    'Value_To_Set = ParseMultiWarehouseQtyOnHand(webClient.DownloadString(PHPStrings(1) & "&function=getmulti&warehouse=1&sku=" & strReturnedSku), strReturnedSku)
        ' '' ''Catch
        ' '' ''    If InStr(Value_To_Set, "e404") > 0 Then Exit Sub
        ' '' ''End Try
        '' '' ''try again later
        ' '' ''If InStr(Value_To_Set, "Failed Call") > 0 Then Exit Sub
        ' '' ''If InStr(Value_To_Set, "e404") > 0 Or Value_To_Set = "" Then
        ' '' ''    '    'check to see if we need to delete rule
        ' '' ''    ''querySync = "UPDATE mverp_db_adjustments_skus_rules Set rule_id = 0 WHERE sku = '" & strReturnedSku & "'"
        ' '' ''    ''Tracing("Setting rule to 0 for sku " & strReturnedSku & " in WVS")
        ' '' ''    Tracing("Problem with rule for sku " & strReturnedSku & " in WVS")
        ' '' ''    cmdsync.CommandText = querySync
        ' '' ''    cmdsync.ExecuteScalar()
        ' '' ''    lblMessage.Text = "Finished Syncing - " & Now()
        ' '' ''    lblMessage.Refresh()
        ' '' ''    'Mark_As_Synced(readerSKURules("id"))
        ' '' ''    Exit Sub
        ' '' ''End If

        If Value_To_Set < 0 Then Value_To_Set = 0
        Dim resultSet As String
        'Set WVS to master inv table
        'doing seperate set bcause WVS never gets rule
        resultSet = webClient.DownloadString(PHPStrings(1) & "&function=get&sku=" & strReturnedSku)

        Dim querySKU As String = "SELECT * FROM mverp_db_adjustments_skus_rules WHERE sku = '" & strReturnedSku & "'"
        cmdsync.CommandText = querySKU
        Dim readerSKURules As MySqlDataReader

        Dim intCounter As Integer = 0
        readerSKURules = cmdsync.ExecuteReader
        If readerSKURules.HasRows Then
            While readerSKURules.Read
                If readerSKURules("rule_id") = 3 Then
                    'get value from WVS
                    lblMessage.Text = "Syncing SKU - " & strReturnedSku & " in " & StoreIDs(readerSKURules("store_id")) & " to value of - " & Value_To_Set
                    lblMessage.Refresh()
                    Try
                        resultSet = webClient.DownloadString(PHPStrings(readerSKURules("store_id")) & "&function=set&sku=" & strReturnedSku & "&qty=" & Value_To_Set)
                    Catch
                        'Clear_Rule(strReturnedSku, readerSKURules("store_id"))
                        lblMessage.Text = "Syncing Stopped"
                        lblMessage.Refresh()
                        flgFailed = False
                        Exit Sub
                    End Try
                End If
                Mark_As_Synced(readerSKURules("id"))
            End While
        End If
        readerSKURules.Close()
        connSync.Close()
        connSync.Dispose()
        lblMessage.Text = "Finished Syncing - " & Now()
        lblMessage.Refresh()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        SyncRules()
    End Sub
    Public Sub UpdateMaster(ByVal New_Qty As Integer, ByVal inSku As String)
        Dim connMasterUpdate As New MySqlConnection(DBConnections("WVS"))
        'Dim queryMasterUpdate As String = "UPDATE mverp_inventory_master SET qty = " & New_Qty & ", last_change ='" & Format(Now, "yyyy/MM/dd HH:mm:ss") & "' WHERE sku = '" & inSku & "'"
        Dim queryMasterUpdate As String = "REPLACE INTO mverp_inventory_master(sku,qty) VALUES('" & inSku & "', " & New_Qty & ")"
        Try
            connMasterUpdate.Open()
            'Tracing("Archiving Record - " & Rec_ID)
            Dim cmdMasterUpdate As New MySqlCommand(queryMasterUpdate, connMasterUpdate)
            cmdMasterUpdate.ExecuteScalar()
            cmdMasterUpdate.Dispose()
        Catch ex As Exception
            SendEmail("Error trying to archive Rec_id - " & inSku & " for UpdateMaster, exiting module")
            flgFailed = True
        Finally
            connMasterUpdate.Close()
            connMasterUpdate.Dispose()
        End Try
    End Sub
    Public Function GetMaster(ByVal inSku As String) As Integer
        Dim connGetMaster As New MySqlConnection(DBConnections("WVS"))
        Dim queryGetMaster As String = "SELECT qty FROM mverp_inventory_master WHERE sku = '" & inSku & "'"
        Dim intReturn As Integer
        Try
            connGetMaster.Open()
            'Tracing("Archiving Record - " & Rec_ID)
            Dim cmdGetMaster As New MySqlCommand(queryGetMaster, connGetMaster)
            intReturn = cmdGetMaster.ExecuteScalar()
            cmdGetMaster.Dispose()
        Catch ex As Exception
            SendEmail("Error trying to archive Rec_id - " & inSku & " for UpdateMaster, exiting module")
            flgFailed = True
        Finally
            connGetMaster.Close()
            connGetMaster.Dispose()
        End Try
        GetMaster = intReturn
    End Function

    ' '' ''Public Sub Clear_Rule(ByVal SKU As String, ByVal StoreID As Integer)
    ' '' ''    Dim connCR As New MySqlConnection(DBConnections("WVS"))
    ' '' ''    connCR.Open()
    ' '' ''    Dim queryCR As String = "UPDATE mverp_db_adjustments_skus_rules SET rule_id = 0 WHERE sku = " & SKU & " and store_id =" & StoreID
    ' '' ''    Dim cmdCR As New MySqlCommand(queryCR, connCR)
    ' '' ''    Try
    ' '' ''        cmdCR.ExecuteScalar()
    ' '' ''    Catch

    ' '' ''    Finally
    ' '' ''        connCR.Close()
    ' '' ''        connCR.Dispose()
    ' '' ''    End Try
    ' '' ''End Sub
End Class
