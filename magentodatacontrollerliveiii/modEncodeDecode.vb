﻿Module modEncodeDecode
    Public Function StrEncode(ByVal s As String, ByVal key As Long, ByVal salt As Boolean) As String
        On Error GoTo Err_StrEncode

        Dim n As Long
        Dim i As Long
        Dim ss As String
        Dim k1 As Long
        Dim k2 As Long
        Dim k3 As Long
        Dim k4 As Long
        Dim t As Long
        Dim sn() As Long

        n = Len(s)
        ss = Space(n)
        ReDim sn(n)

        k1 = 11 + (key Mod 233) : k2 = 7 + (key Mod 239)
        k3 = 5 + (key Mod 241) : k4 = 3 + (key Mod 251)

        For i = 1 To n : sn(i) = Asc(Mid(s, i, 1)) : Next i

        For i = 2 To n : sn(i) = sn(i) Xor sn(i - 1) Xor ((k1 * sn(i - 1)) Mod 256) : Next
        For i = n - 1 To 1 Step -1 : sn(i) = sn(i) Xor sn(i + 1) Xor (k2 * sn(i + 1)) Mod 256 : Next
        For i = 3 To n : sn(i) = sn(i) Xor sn(i - 2) Xor (k3 * sn(i - 1)) Mod 256 : Next
        For i = n - 2 To 1 Step -1 : sn(i) = sn(i) Xor sn(i + 2) Xor (k4 * sn(i + 1)) Mod 256 : Next
        For i = 1 To n : Mid(ss, i, 1) = Chr(sn(i)) : Next i
        StrEncode = ss

Exit_StrEncode:
        Exit Function

Err_StrEncode:
        MsgBox(Err.Description, , "StrEncode Error")
        GoTo Exit_StrEncode
        Resume

    End Function
    Public Function StrDecode(ByVal s As String, ByVal key As Long, ByVal salt As Boolean) As String

        On Error GoTo Err_StrDecode

        Dim n As Long
        Dim i As Long
        Dim ss As String
        Dim k1 As Long
        Dim k2 As Long
        Dim k3 As Long
        Dim k4 As Long
        Dim sn() As Long

        n = Len(s)
        ss = Space(n)

        ReDim sn(n) 'As Long

        k1 = 11 + (key Mod 233) : k2 = 7 + (key Mod 239)
        k3 = 5 + (key Mod 241) : k4 = 3 + (key Mod 251)

        For i = 1 To n : sn(i) = Asc(Mid(s, i, 1)) : Next

        For i = 1 To n - 2 : sn(i) = sn(i) Xor sn(i + 2) Xor (k4 * sn(i + 1)) Mod 256 : Next
        For i = n To 3 Step -1 : sn(i) = sn(i) Xor sn(i - 2) Xor (k3 * sn(i - 1)) Mod 256 : Next
        For i = 1 To n - 1 : sn(i) = sn(i) Xor sn(i + 1) Xor (k2 * sn(i + 1)) Mod 256 : Next
        For i = n To 2 Step -1 : sn(i) = sn(i) Xor sn(i - 1) Xor (k1 * sn(i - 1)) Mod 256 : Next

        For i = 1 To n : Mid(ss, i, 1) = Chr(sn(i)) : Next i

        If salt Then StrDecode = Mid(ss, 3, Len(ss) - 4) Else StrDecode = ss

Exit_StrDecode:
        Exit Function

Err_StrDecode:
        MsgBox(Err.Description, , "StrDecode Error")
        GoTo Exit_StrDecode
        Resume

    End Function
End Module
