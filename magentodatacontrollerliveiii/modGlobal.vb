﻿Imports System.Net
Imports System.Net.Mail
Imports System.IO
Imports MySql.Data.MySqlClient

Module modGlobal
    Public flgFailed As Boolean = False
    Public flgRestarted As Boolean = False
    Public DatabaseList As New List(Of Database)()
    'live
    Public connstr As String = "Database=maddata_wvs;Data Source=madverp.com;User Id=maddata_master;Password=L0v32V4P3"
    'dev
    'Public connstr As String = "Database=madvapes_wvs;Data Source=test.madvapes.com;User Id=madvapes_data;Password=L0v32V4P3"
    Public FromDir As String = "C:\DBControllerLive"
    Public ToDir As String = "C:\DBControllerLiveArchive"
    Public intSKUConnectionLoops As Integer = 0
    Public intconnRemoteDatabasestr_ProcessFiles As Integer = 0
    'Added 2016-09-16
    Public DBConnections As New Hashtable
    Public PHPStrings As New Hashtable
    Public StoreIDs As New Hashtable
    Public StoreNames As New Hashtable
    Public Sub Tracing(ByVal msgToWrite As String)
        If (Not System.IO.Directory.Exists("C:\DBControllerLiveArchive")) Then
            System.IO.Directory.CreateDirectory("C:\DBControllerLiveArchive")
        End If
        Dim tm As System.DateTime
        Dim file1 As FileInfo = New FileInfo("C:\DBControllerLiveArchive\" & Format(Now, "yyyy-MM-dd") & ".log")
        Dim sWriter As StreamWriter = file1.AppendText()
        tm = Now
        sWriter.WriteLine(tm & ":" & msgToWrite)
        sWriter.Close()
    End Sub
    Public Sub SendEmail(Optional ByVal Message As String = "")
        Try
            Dim SmtpServer As New SmtpClient()
            Dim mail As New MailMessage()
            SmtpServer.EnableSsl = True
            SmtpServer.Credentials = New Net.NetworkCredential("madvapes.ageverify@gmail.com", "AgeVerify!@#")
            SmtpServer.Port = 587
            SmtpServer.Host = "smtp.gmail.com"
            mail = New MailMessage()
            mail.From = New MailAddress("madvapes." & Application.ProductName & "@gmail.com")
            mail.To.Add("clt1dpw@yahoo.com")
            mail.To.Add("jason.ehmke@madvapes.com")
            mail.To.Add("perry.wright@madvapes.com")
            mail.Subject = "Database Connection"
            If Message = "" Then
                mail.Body = "The Magento Data Controller database connection has failed"
            Else
                mail.Body = Message & vbCrLf & My.Computer.Name
            End If
            SmtpServer.Send(mail)
        Catch ex As Exception
            If InStr(ex.ToString, "Mailbox unavailable") > 0 Then
                Tracing(Message)
            Else
                MsgBox(ex.ToString)
            End If

        End Try
    End Sub
    Public Sub CreateRestartFile()
        If (Not System.IO.Directory.Exists("C:\DBControllerLiveArchive")) Then
            System.IO.Directory.CreateDirectory("C:\DBControllerLiveArchive")
        End If
        Dim tm As System.DateTime
        Dim file1 As FileInfo = New FileInfo("C:\DBControllerLiveArchive\Restarted.log")
        Dim sWriter As StreamWriter = file1.AppendText()
        tm = Now
        sWriter.WriteLine(tm)
        sWriter.Close()
    End Sub
End Module
